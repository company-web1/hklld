package com.regex.admin.service.web.impl;
import java.util.List;
import com.regex.admin.dao.web.AboutUsMapper;
import com.regex.admin.common.dto.web.AboutUsDTO;
import com.regex.admin.common.util.Assist;
import com.regex.admin.service.web.IAboutUsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class AboutUsServiceImpl implements IAboutUsService{
    @Autowired
    private AboutUsMapper aboutUsMapper;
    @Override
    public long getAboutUsDTORowCount(Assist assist){
        return aboutUsMapper.getAboutUsDTORowCount(assist);
    }
    @Override
    public List<AboutUsDTO> selectAboutUsDTO(Assist assist){
        return aboutUsMapper.selectAboutUsDTO(assist);
    }
    @Override
    public AboutUsDTO selectAboutUsDTOByObj(AboutUsDTO obj){
        return aboutUsMapper.selectAboutUsDTOByObj(obj);
    }
    @Override
    public AboutUsDTO selectAboutUsDTOById(Long id){
        return aboutUsMapper.selectAboutUsDTOById(id);
    }
    @Override
    public int insertAboutUsDTO(AboutUsDTO value){
        return aboutUsMapper.insertAboutUsDTO(value);
    }
    @Override
    public int insertNonEmptyAboutUsDTO(AboutUsDTO value){
        return aboutUsMapper.insertNonEmptyAboutUsDTO(value);
    }
    @Override
    public int insertAboutUsDTOByBatch(List<AboutUsDTO> value){
        return aboutUsMapper.insertAboutUsDTOByBatch(value);
    }
    @Override
    public int deleteAboutUsDTOById(Long id){
        return aboutUsMapper.deleteAboutUsDTOById(id);
    }
    @Override
    public int deleteAboutUsDTO(Assist assist){
        return aboutUsMapper.deleteAboutUsDTO(assist);
    }
    @Override
    public int updateAboutUsDTOById(AboutUsDTO enti){
        return aboutUsMapper.updateAboutUsDTOById(enti);
    }
    @Override
    public int updateAboutUsDTO(AboutUsDTO value, Assist assist){
        return aboutUsMapper.updateAboutUsDTO(value,assist);
    }
    @Override
    public int updateNonEmptyAboutUsDTOById(AboutUsDTO enti){
        return aboutUsMapper.updateNonEmptyAboutUsDTOById(enti);
    }
    @Override
    public int updateNonEmptyAboutUsDTO(AboutUsDTO value, Assist assist){
        return aboutUsMapper.updateNonEmptyAboutUsDTO(value,assist);
    }

    public AboutUsMapper getIAboutUsDao() {
        return this.aboutUsMapper;
    }

    public void setIAboutUsDao(AboutUsMapper aboutUsMapper) {
        this.aboutUsMapper = aboutUsMapper;
    }

}