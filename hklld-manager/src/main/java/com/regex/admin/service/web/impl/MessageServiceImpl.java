package com.regex.admin.service.web.impl;
import java.util.List;
import com.regex.admin.dao.web.MessageMapper;
import com.regex.admin.common.dto.web.MessageDTO;
import com.regex.admin.common.util.Assist;
import com.regex.admin.service.web.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class MessageServiceImpl implements IMessageService{
    @Autowired
    private MessageMapper messageMapper;
    @Override
    public long getMessageDTORowCount(Assist assist){
        return messageMapper.getMessageDTORowCount(assist);
    }
    @Override
    public List<MessageDTO> selectMessageDTO(Assist assist){
        return messageMapper.selectMessageDTO(assist);
    }
    @Override
    public MessageDTO selectMessageDTOByObj(MessageDTO obj){
        return messageMapper.selectMessageDTOByObj(obj);
    }
    @Override
    public MessageDTO selectMessageDTOById(Long id){
        return messageMapper.selectMessageDTOById(id);
    }
    @Override
    public int insertMessageDTO(MessageDTO value){
        return messageMapper.insertMessageDTO(value);
    }
    @Override
    public int insertNonEmptyMessageDTO(MessageDTO value){
        return messageMapper.insertNonEmptyMessageDTO(value);
    }
    @Override
    public int insertMessageDTOByBatch(List<MessageDTO> value){
        return messageMapper.insertMessageDTOByBatch(value);
    }
    @Override
    public int deleteMessageDTOById(Long id){
        return messageMapper.deleteMessageDTOById(id);
    }
    @Override
    public int deleteMessageDTO(Assist assist){
        return messageMapper.deleteMessageDTO(assist);
    }
    @Override
    public int updateMessageDTOById(MessageDTO enti){
        return messageMapper.updateMessageDTOById(enti);
    }
    @Override
    public int updateMessageDTO(MessageDTO value, Assist assist){
        return messageMapper.updateMessageDTO(value,assist);
    }
    @Override
    public int updateNonEmptyMessageDTOById(MessageDTO enti){
        return messageMapper.updateNonEmptyMessageDTOById(enti);
    }
    @Override
    public int updateNonEmptyMessageDTO(MessageDTO value, Assist assist){
        return messageMapper.updateNonEmptyMessageDTO(value,assist);
    }

    public MessageMapper getIMessageDao() {
        return this.messageMapper;
    }

    public void setIMessageDao(MessageMapper messageMapper) {
        this.messageMapper = messageMapper;
    }

}