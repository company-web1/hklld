package com.regex.admin.service.web.impl;
import java.util.List;
import com.regex.admin.dao.web.NewsMapper;
import com.regex.admin.common.dto.web.NewsDTO;
import com.regex.admin.common.util.Assist;
import com.regex.admin.service.web.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class NewsServiceImpl implements INewsService{
    @Autowired
    private NewsMapper newsMapper;
    @Override
    public long getNewsDTORowCount(Assist assist){
        return newsMapper.getNewsDTORowCount(assist);
    }
    @Override
    public List<NewsDTO> selectNewsDTO(Assist assist){
        return newsMapper.selectNewsDTO(assist);
    }
    @Override
    public NewsDTO selectNewsDTOByObj(NewsDTO obj){
        return newsMapper.selectNewsDTOByObj(obj);
    }
    @Override
    public NewsDTO selectNewsDTOById(Long id){
        return newsMapper.selectNewsDTOById(id);
    }
    @Override
    public int insertNewsDTO(NewsDTO value){
        return newsMapper.insertNewsDTO(value);
    }
    @Override
    public int insertNonEmptyNewsDTO(NewsDTO value){
        return newsMapper.insertNonEmptyNewsDTO(value);
    }
    @Override
    public int insertNewsDTOByBatch(List<NewsDTO> value){
        return newsMapper.insertNewsDTOByBatch(value);
    }
    @Override
    public int deleteNewsDTOById(Long id){
        return newsMapper.deleteNewsDTOById(id);
    }
    @Override
    public int deleteNewsDTO(Assist assist){
        return newsMapper.deleteNewsDTO(assist);
    }
    @Override
    public int updateNewsDTOById(NewsDTO enti){
        return newsMapper.updateNewsDTOById(enti);
    }
    @Override
    public int updateNewsDTO(NewsDTO value, Assist assist){
        return newsMapper.updateNewsDTO(value,assist);
    }
    @Override
    public int updateNonEmptyNewsDTOById(NewsDTO enti){
        return newsMapper.updateNonEmptyNewsDTOById(enti);
    }
    @Override
    public int updateNonEmptyNewsDTO(NewsDTO value, Assist assist){
        return newsMapper.updateNonEmptyNewsDTO(value,assist);
    }

    public NewsMapper getINewsDao() {
        return this.newsMapper;
    }

    public void setINewsDao(NewsMapper newsMapper) {
        this.newsMapper = newsMapper;
    }

}