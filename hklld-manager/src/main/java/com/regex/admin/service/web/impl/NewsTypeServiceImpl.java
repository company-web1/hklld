package com.regex.admin.service.web.impl;
import java.util.List;
import com.regex.admin.dao.web.NewsTypeMapper;
import com.regex.admin.common.dto.web.NewsTypeDTO;
import com.regex.admin.common.util.Assist;
import com.regex.admin.service.web.INewsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class NewsTypeServiceImpl implements INewsTypeService{
    @Autowired
    private NewsTypeMapper newsTypeMapper;
    @Override
    public long getNewsTypeDTORowCount(Assist assist){
        return newsTypeMapper.getNewsTypeDTORowCount(assist);
    }
    @Override
    public List<NewsTypeDTO> selectNewsTypeDTO(Assist assist){
        return newsTypeMapper.selectNewsTypeDTO(assist);
    }
    @Override
    public NewsTypeDTO selectNewsTypeDTOByObj(NewsTypeDTO obj){
        return newsTypeMapper.selectNewsTypeDTOByObj(obj);
    }
    @Override
    public NewsTypeDTO selectNewsTypeDTOById(Long id){
        return newsTypeMapper.selectNewsTypeDTOById(id);
    }
    @Override
    public int insertNewsTypeDTO(NewsTypeDTO value){
        return newsTypeMapper.insertNewsTypeDTO(value);
    }
    @Override
    public int insertNonEmptyNewsTypeDTO(NewsTypeDTO value){
        return newsTypeMapper.insertNonEmptyNewsTypeDTO(value);
    }
    @Override
    public int insertNewsTypeDTOByBatch(List<NewsTypeDTO> value){
        return newsTypeMapper.insertNewsTypeDTOByBatch(value);
    }
    @Override
    public int deleteNewsTypeDTOById(Long id){
        return newsTypeMapper.deleteNewsTypeDTOById(id);
    }
    @Override
    public int deleteNewsTypeDTO(Assist assist){
        return newsTypeMapper.deleteNewsTypeDTO(assist);
    }
    @Override
    public int updateNewsTypeDTOById(NewsTypeDTO enti){
        return newsTypeMapper.updateNewsTypeDTOById(enti);
    }
    @Override
    public int updateNewsTypeDTO(NewsTypeDTO value, Assist assist){
        return newsTypeMapper.updateNewsTypeDTO(value,assist);
    }
    @Override
    public int updateNonEmptyNewsTypeDTOById(NewsTypeDTO enti){
        return newsTypeMapper.updateNonEmptyNewsTypeDTOById(enti);
    }
    @Override
    public int updateNonEmptyNewsTypeDTO(NewsTypeDTO value, Assist assist){
        return newsTypeMapper.updateNonEmptyNewsTypeDTO(value,assist);
    }

    public NewsTypeMapper getINewsTypeDao() {
        return this.newsTypeMapper;
    }

    public void setINewsTypeDao(NewsTypeMapper newsTypeMapper) {
        this.newsTypeMapper = newsTypeMapper;
    }

}