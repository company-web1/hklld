package com.regex.admin.service.web.impl;
import java.util.List;
import com.regex.admin.dao.web.BannerMapper;
import com.regex.admin.common.dto.web.BannerDTO;
import com.regex.admin.common.util.Assist;
import com.regex.admin.service.web.IBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class BannerServiceImpl implements IBannerService{
    @Autowired
    private BannerMapper bannerMapper;
    @Override
    public long getBannerDTORowCount(Assist assist){
        return bannerMapper.getBannerDTORowCount(assist);
    }
    @Override
    public List<BannerDTO> selectBannerDTO(Assist assist){
        return bannerMapper.selectBannerDTO(assist);
    }
    @Override
    public BannerDTO selectBannerDTOByObj(BannerDTO obj){
        return bannerMapper.selectBannerDTOByObj(obj);
    }
    @Override
    public BannerDTO selectBannerDTOById(Long id){
        return bannerMapper.selectBannerDTOById(id);
    }
    @Override
    public int insertBannerDTO(BannerDTO value){
        return bannerMapper.insertBannerDTO(value);
    }
    @Override
    public int insertNonEmptyBannerDTO(BannerDTO value){
        return bannerMapper.insertNonEmptyBannerDTO(value);
    }
    @Override
    public int insertBannerDTOByBatch(List<BannerDTO> value){
        return bannerMapper.insertBannerDTOByBatch(value);
    }
    @Override
    public int deleteBannerDTOById(Long id){
        return bannerMapper.deleteBannerDTOById(id);
    }
    @Override
    public int deleteBannerDTO(Assist assist){
        return bannerMapper.deleteBannerDTO(assist);
    }
    @Override
    public int updateBannerDTOById(BannerDTO enti){
        return bannerMapper.updateBannerDTOById(enti);
    }
    @Override
    public int updateBannerDTO(BannerDTO value, Assist assist){
        return bannerMapper.updateBannerDTO(value,assist);
    }
    @Override
    public int updateNonEmptyBannerDTOById(BannerDTO enti){
        return bannerMapper.updateNonEmptyBannerDTOById(enti);
    }
    @Override
    public int updateNonEmptyBannerDTO(BannerDTO value, Assist assist){
        return bannerMapper.updateNonEmptyBannerDTO(value,assist);
    }

    public BannerMapper getIBannerDao() {
        return this.bannerMapper;
    }

    public void setIBannerDao(BannerMapper bannerMapper) {
        this.bannerMapper = bannerMapper;
    }

}