package com.regex.admin.service.web.impl;
import java.util.List;
import com.regex.admin.dao.web.PartnerMapper;
import com.regex.admin.common.dto.web.PartnerDTO;
import com.regex.admin.common.util.Assist;
import com.regex.admin.service.web.IPartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class PartnerServiceImpl implements IPartnerService{
    @Autowired
    private PartnerMapper partnerMapper;
    @Override
    public long getPartnerDTORowCount(Assist assist){
        return partnerMapper.getPartnerDTORowCount(assist);
    }
    @Override
    public List<PartnerDTO> selectPartnerDTO(Assist assist){
        return partnerMapper.selectPartnerDTO(assist);
    }
    @Override
    public PartnerDTO selectPartnerDTOByObj(PartnerDTO obj){
        return partnerMapper.selectPartnerDTOByObj(obj);
    }
    @Override
    public PartnerDTO selectPartnerDTOById(Long id){
        return partnerMapper.selectPartnerDTOById(id);
    }
    @Override
    public int insertPartnerDTO(PartnerDTO value){
        return partnerMapper.insertPartnerDTO(value);
    }
    @Override
    public int insertNonEmptyPartnerDTO(PartnerDTO value){
        return partnerMapper.insertNonEmptyPartnerDTO(value);
    }
    @Override
    public int insertPartnerDTOByBatch(List<PartnerDTO> value){
        return partnerMapper.insertPartnerDTOByBatch(value);
    }
    @Override
    public int deletePartnerDTOById(Long id){
        return partnerMapper.deletePartnerDTOById(id);
    }
    @Override
    public int deletePartnerDTO(Assist assist){
        return partnerMapper.deletePartnerDTO(assist);
    }
    @Override
    public int updatePartnerDTOById(PartnerDTO enti){
        return partnerMapper.updatePartnerDTOById(enti);
    }
    @Override
    public int updatePartnerDTO(PartnerDTO value, Assist assist){
        return partnerMapper.updatePartnerDTO(value,assist);
    }
    @Override
    public int updateNonEmptyPartnerDTOById(PartnerDTO enti){
        return partnerMapper.updateNonEmptyPartnerDTOById(enti);
    }
    @Override
    public int updateNonEmptyPartnerDTO(PartnerDTO value, Assist assist){
        return partnerMapper.updateNonEmptyPartnerDTO(value,assist);
    }

    public PartnerMapper getIPartnerDao() {
        return this.partnerMapper;
    }

    public void setIPartnerDao(PartnerMapper partnerMapper) {
        this.partnerMapper = partnerMapper;
    }

}