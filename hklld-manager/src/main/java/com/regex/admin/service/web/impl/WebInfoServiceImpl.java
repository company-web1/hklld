package com.regex.admin.service.web.impl;
import java.util.List;
import com.regex.admin.dao.web.WebInfoMapper;
import com.regex.admin.common.dto.web.WebInfoDTO;
import com.regex.admin.common.util.Assist;
import com.regex.admin.service.web.IWebInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class WebInfoServiceImpl implements IWebInfoService{
    @Autowired
    private WebInfoMapper webInfoMapper;
    @Override
    public long getWebInfoDTORowCount(Assist assist){
        return webInfoMapper.getWebInfoDTORowCount(assist);
    }
    @Override
    public List<WebInfoDTO> selectWebInfoDTO(Assist assist){
        return webInfoMapper.selectWebInfoDTO(assist);
    }
    @Override
    public WebInfoDTO selectWebInfoDTOByObj(WebInfoDTO obj){
        return webInfoMapper.selectWebInfoDTOByObj(obj);
    }
    @Override
    public WebInfoDTO selectWebInfoDTOById(Long id){
        return webInfoMapper.selectWebInfoDTOById(id);
    }
    @Override
    public int insertWebInfoDTO(WebInfoDTO value){
        return webInfoMapper.insertWebInfoDTO(value);
    }
    @Override
    public int insertNonEmptyWebInfoDTO(WebInfoDTO value){
        return webInfoMapper.insertNonEmptyWebInfoDTO(value);
    }
    @Override
    public int insertWebInfoDTOByBatch(List<WebInfoDTO> value){
        return webInfoMapper.insertWebInfoDTOByBatch(value);
    }
    @Override
    public int deleteWebInfoDTOById(Long id){
        return webInfoMapper.deleteWebInfoDTOById(id);
    }
    @Override
    public int deleteWebInfoDTO(Assist assist){
        return webInfoMapper.deleteWebInfoDTO(assist);
    }
    @Override
    public int updateWebInfoDTOById(WebInfoDTO enti){
        return webInfoMapper.updateWebInfoDTOById(enti);
    }
    @Override
    public int updateWebInfoDTO(WebInfoDTO value, Assist assist){
        return webInfoMapper.updateWebInfoDTO(value,assist);
    }
    @Override
    public int updateNonEmptyWebInfoDTOById(WebInfoDTO enti){
        return webInfoMapper.updateNonEmptyWebInfoDTOById(enti);
    }
    @Override
    public int updateNonEmptyWebInfoDTO(WebInfoDTO value, Assist assist){
        return webInfoMapper.updateNonEmptyWebInfoDTO(value,assist);
    }

    public WebInfoMapper getIWebInfoDao() {
        return this.webInfoMapper;
    }

    public void setIWebInfoDao(WebInfoMapper webInfoMapper) {
        this.webInfoMapper = webInfoMapper;
    }

}