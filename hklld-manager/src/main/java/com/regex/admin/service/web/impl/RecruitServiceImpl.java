package com.regex.admin.service.web.impl;
import java.util.List;
import com.regex.admin.dao.web.RecruitMapper;
import com.regex.admin.common.dto.web.RecruitDTO;
import com.regex.admin.common.util.Assist;
import com.regex.admin.service.web.IRecruitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class RecruitServiceImpl implements IRecruitService{
    @Autowired
    private RecruitMapper recruitMapper;
    @Override
    public long getRecruitDTORowCount(Assist assist){
        return recruitMapper.getRecruitDTORowCount(assist);
    }
    @Override
    public List<RecruitDTO> selectRecruitDTO(Assist assist){
        return recruitMapper.selectRecruitDTO(assist);
    }
    @Override
    public RecruitDTO selectRecruitDTOByObj(RecruitDTO obj){
        return recruitMapper.selectRecruitDTOByObj(obj);
    }
    @Override
    public RecruitDTO selectRecruitDTOById(Long id){
        return recruitMapper.selectRecruitDTOById(id);
    }
    @Override
    public int insertRecruitDTO(RecruitDTO value){
        return recruitMapper.insertRecruitDTO(value);
    }
    @Override
    public int insertNonEmptyRecruitDTO(RecruitDTO value){
        return recruitMapper.insertNonEmptyRecruitDTO(value);
    }
    @Override
    public int insertRecruitDTOByBatch(List<RecruitDTO> value){
        return recruitMapper.insertRecruitDTOByBatch(value);
    }
    @Override
    public int deleteRecruitDTOById(Long id){
        return recruitMapper.deleteRecruitDTOById(id);
    }
    @Override
    public int deleteRecruitDTO(Assist assist){
        return recruitMapper.deleteRecruitDTO(assist);
    }
    @Override
    public int updateRecruitDTOById(RecruitDTO enti){
        return recruitMapper.updateRecruitDTOById(enti);
    }
    @Override
    public int updateRecruitDTO(RecruitDTO value, Assist assist){
        return recruitMapper.updateRecruitDTO(value,assist);
    }
    @Override
    public int updateNonEmptyRecruitDTOById(RecruitDTO enti){
        return recruitMapper.updateNonEmptyRecruitDTOById(enti);
    }
    @Override
    public int updateNonEmptyRecruitDTO(RecruitDTO value, Assist assist){
        return recruitMapper.updateNonEmptyRecruitDTO(value,assist);
    }

    public RecruitMapper getIRecruitDao() {
        return this.recruitMapper;
    }

    public void setIRecruitDao(RecruitMapper recruitMapper) {
        this.recruitMapper = recruitMapper;
    }

}