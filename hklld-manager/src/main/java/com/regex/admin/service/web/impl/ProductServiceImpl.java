package com.regex.admin.service.web.impl;
import java.util.List;
import com.regex.admin.dao.web.ProductMapper;
import com.regex.admin.common.dto.web.ProductDTO;
import com.regex.admin.common.util.Assist;
import com.regex.admin.service.web.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class ProductServiceImpl implements IProductService{
    @Autowired
    private ProductMapper productMapper;
    @Override
    public long getProductDTORowCount(Assist assist){
        return productMapper.getProductDTORowCount(assist);
    }
    @Override
    public List<ProductDTO> selectProductDTO(Assist assist){
        return productMapper.selectProductDTO(assist);
    }
    @Override
    public ProductDTO selectProductDTOByObj(ProductDTO obj){
        return productMapper.selectProductDTOByObj(obj);
    }
    @Override
    public ProductDTO selectProductDTOById(Long id){
        return productMapper.selectProductDTOById(id);
    }
    @Override
    public int insertProductDTO(ProductDTO value){
        return productMapper.insertProductDTO(value);
    }
    @Override
    public int insertNonEmptyProductDTO(ProductDTO value){
        return productMapper.insertNonEmptyProductDTO(value);
    }
    @Override
    public int insertProductDTOByBatch(List<ProductDTO> value){
        return productMapper.insertProductDTOByBatch(value);
    }
    @Override
    public int deleteProductDTOById(Long id){
        return productMapper.deleteProductDTOById(id);
    }
    @Override
    public int deleteProductDTO(Assist assist){
        return productMapper.deleteProductDTO(assist);
    }
    @Override
    public int updateProductDTOById(ProductDTO enti){
        return productMapper.updateProductDTOById(enti);
    }
    @Override
    public int updateProductDTO(ProductDTO value, Assist assist){
        return productMapper.updateProductDTO(value,assist);
    }
    @Override
    public int updateNonEmptyProductDTOById(ProductDTO enti){
        return productMapper.updateNonEmptyProductDTOById(enti);
    }
    @Override
    public int updateNonEmptyProductDTO(ProductDTO value, Assist assist){
        return productMapper.updateNonEmptyProductDTO(value,assist);
    }

    public ProductMapper getIProductDao() {
        return this.productMapper;
    }

    public void setIProductDao(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

}