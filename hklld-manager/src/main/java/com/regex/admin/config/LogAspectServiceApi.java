package com.regex.admin.config;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import oracle.jrockit.jfr.VMJFR;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
@Slf4j
public class LogAspectServiceApi {
	private JSONObject jsonObject = new JSONObject();

	@Pointcut(value = "execution(public * com.regex.admin.web.controller..*.*(..))")
	private void controllerAspect() {
	}
	// 日志切面
	@Before(value = "controllerAspect()")
	public void methodBefore(JoinPoint joinPoint) {
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		HttpServletRequest request = requestAttributes.getRequest();
		log.info("=====>> request api service methodBefore log:");
		try {
			log.info("=====>>url :" + request.getRequestURL().toString()+",methodType :"+request.getMethod());
			log.info("=====>>method :" + joinPoint.getSignature());
		//	log.info("=====>>parameters :" + jsonObject.toJSONString(joinPoint.getArgs()));
		} catch (Exception e) {
			log.error("=====>> LogAspectServiceApi.class methodBefore() ### ERROR:", e);
		}
	}
	@AfterReturning(returning = "result", pointcut = "controllerAspect()")
	public void methodAfterReturing(Object result) {
		try {
			log.info("=====>>response :" + jsonObject.toJSONString(result));
		} catch (Exception e) {
			log.error("=========LogAspectServiceApi.class methodAfterReturing() ### ERROR:", e);
		}
	}
}
