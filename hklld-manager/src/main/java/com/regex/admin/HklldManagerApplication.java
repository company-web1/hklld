package com.regex.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan({"com.regex.admin.dao.sys","com.regex.admin.dao.web"})
@EnableSwagger2
@ImportResource(locations={"classpath:/context/spring-ehcache.xml",
"classpath:/context/applicationContext-common.xml"})
public class HklldManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(HklldManagerApplication.class, args);
    }

}
